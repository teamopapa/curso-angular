import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';
import { AppState, db } from '../app.module';
import { DestinoViaje } from './destino-viaje.model';
import { ElegidoFavoritoAction, NuevoDestinoAction } from './destinos-viajes-state.model';

@Injectable()
export class DestinosApiClient {
  [x: string]: any;
  constructor(public store: Store<AppState>) {
  }
  add(d:DestinoViaje){
    this.store.dispatch(new NuevoDestinoAction(d));
    const myDb = db;
    myDb.destinos.add(d);
    console.log('todos los destinos de la db!');
    myDb.destinos.toArray().then(destinos => console.log(destinos))
  }

  getById(id: String): DestinoViaje {
    return this.destinos.filter(function(d) { return d.id.toString() === id; })[0];
  }

  getAll(): DestinoViaje[] {
    return this.destinos;
  }

 elegir(d: DestinoViaje) {
 this.store.dispatch(new ElegidoFavoritoAction(d));
 }

}
